﻿using System.Collections.Generic;



Console.WriteLine("Введите количество месяцев");
var months = int.Parse(Console.ReadLine());

Console.WriteLine(FibonacciRabbits(months));

int FibonacciRabbits(int limit)
{

    int firstElem = 0;
    int secondElem = 1;
    for (int i = 1; i < limit; i++)
    {
        int t = secondElem;
        secondElem += firstElem;
        firstElem = t;
    }
    return secondElem;
}