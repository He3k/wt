﻿using System.Linq;

Console.WriteLine("Введите строку");
string inputStr = Console.ReadLine();

if (IfPolyndrom(inputStr))
{
    Console.WriteLine("Палиндром");
}
else
{
    Console.WriteLine("Не Палиндром");
}

bool IfPolyndrom(string str)
{
    int len = str.Length;
    for (int i = 0; i < str.Length; i++)
    {
        if (str.ElementAt(i) != str.ElementAt(len - 1 - i))
        {
            return false;
        }
    }
    return true;
}