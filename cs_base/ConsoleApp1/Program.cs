﻿// See https://aka.ms/new-console-template for more information


double convertCelcium(double degrees, string ToUnits){
    switch(ToUnits){
        case "F":
            return (degrees * 9/5) + 32;
        case "K":
            return degrees + 273.0;

    }
    return degrees;
}

double convertFahren(double degrees, string ToUnits){
    switch(ToUnits){
        case "C":
            return (degrees - 32) *5/9;
        case "K":
            return (degrees + 459.67) * 5/9;

    }
    return degrees;
}


double convertKelvin(double degrees, string ToUnits){
    switch(ToUnits){
        case "C":
            return degrees - 273;
        case "F":
            return (degrees - 459.67) * 9/5;

    }
    return degrees;
}

double convertDegrees(double degrees, string FromUnits, string ToUnits){
    
    switch(FromUnits){
        case "C":{
            return convertCelcium(degrees,ToUnits);
        }
        case "F":{
            return convertFahren(degrees,ToUnits);
        }
        case "K":{
            return convertKelvin(degrees,ToUnits);
        }
    }
    return degrees;
}
double degrees  = double.Parse(Console.ReadLine());
Console.WriteLine("Введите исходные единицы измерения:");
string FromUnits = Console.ReadLine().ToUpper();
Console.WriteLine("Введите желаемые единицы измерения");
string ToUnits = Console.ReadLine().ToUpper();
Console.WriteLine(""+ convertDegrees(degrees,FromUnits,ToUnits));
