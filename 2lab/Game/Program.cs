﻿
using System.Security.Authentication.ExtendedProtection;
using System.Collections.Generic;

using System.Text.Json.Serialization;
using System.Text.Json;
using System.Data;

var ValeraAdventure = new Game();
ValeraAdventure.MainMenu();


public class StarterConfig
{
    public Stats defaultStats { get; set; }
    public int MIN_FATIGUE { get; set; }
    public int MAX_FATIGUE { get; set; }
    public int MIN_MANA { get; set; }
    public int MAX_MANA { get; set; }
    public int MIN_HEALTH { get; set; }
    public int MAX_HEALTH { get; set; }
    public int MIN_CHEER { get; set; }
    public int MAX_CHEER { get; set; }


}
public class AtomRecord
{

    public string text { get; set; }
    public Dictionary<string, string> Cond { get; set; }
    public Stats Stats { get; set; }
}

public class ActionConfig
{
    public string title { get; set; }
    // [JsonPropertyName("main")]
    public AtomRecord main { get; set; }
    // [JsonPropertyName("Extra")]
    public List<AtomRecord> Extra { get; set; }
}
public class Config
{
    public Dictionary<String, ActionConfig> data;
}

public class Game
{
    enum PChoice
    {
        Wait = 0,
        Work = 1,
        Bar = 4,
        Nature = 2,
        SoapWine = 3,
        MarginalDrink = 5,
        Metro = 6,
        Sleep = 7,
        SaveGame = 8,
        Finish = 9,
    }
    public StarterConfig defaultConfig;
    private void ReadDefaultConfig()
    {
        string json = File.ReadAllText("default.json");
        defaultConfig = JsonSerializer.Deserialize<StarterConfig>(json)!;

    }

    // Главный герой игры (Valera)
    public Valera MainCharacter { get; private set; }
    private Dictionary<String, ActionConfig> RawConfig;

    // Конструктор класса Game
    public Game()
    {
        // Создаем экземпляр главного героя Valera и инициализируем его параметры
        ReadDefaultConfig();
        MainCharacter = new Valera();
        MainCharacter.DisplayInfo();
    }

    private void DrawMainMenu()
    {
        Console.WriteLine("Главное Меню:");
        Console.WriteLine("1.Новая игра");
        Console.WriteLine("2.Загрузить игру");
        Console.WriteLine("3.Выход");

    }

    private Dictionary<string, string> StsatsToDict(Stats stats)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        dict["Мана"] = stats.Mana.ToString();
        dict["Радость"] = stats.Cheerfulness.ToString();
        dict["Усталость"] = stats.Fatigue.ToString();
        dict["Деньги"] = stats.Health.ToString();
        dict["Здоровье"] = stats.Health.ToString();
        return dict;
    }

    private Stats DictToStats(Dictionary<string, string> dict)
    {
        Stats s = new Stats();
        s.Mana = int.Parse(dict["Мана"]);
        s.Cheerfulness = int.Parse(dict["Радость"]);
        s.Fatigue = int.Parse(dict["Усталость"]);
        s.Health = int.Parse(dict["Здоровье"]);
        s.Money = decimal.Parse(dict["Деньги"]);

        return s;

    }
    private bool checkStat(string rule, string stat)
    {
        int signIndex = -1;
        int digitIndex = -1;

        for (int i = 0; i < rule.Length; ++i)
        {
            if (rule[i] == '>' || rule[i] == '<')
            {
                signIndex = i;
            }
            else if (char.IsDigit(rule[i]))
            {
                digitIndex = i;
                break;
            }
        }

        if (digitIndex == -1 || signIndex == -1)
        {
            Console.WriteLine("wrong format " + rule);
            return false;
        };
        Stats actual = new Stats();
        Stats compare = new Stats();

        if (stat.StartsWith("Mana"))
        {
            actual.setStats(initialMana: MainCharacter.GetStats().Mana);
            int digStat = int.Parse(rule.Substring(digitIndex));
            compare.setStats(initialMana: digStat);

        }
        else if (stat.StartsWith("Health"))
        {
            actual.setStats(initialHealth: MainCharacter.GetStats().Health);
            int digStat = int.Parse(rule.Substring(digitIndex));
            compare.setStats(initialHealth: digStat);
        }
        else if (stat.StartsWith("Cheerfulness"))
        {
            actual.setStats(initialCheerfulness: MainCharacter.GetStats().Cheerfulness);
            int digStat = int.Parse(rule.Substring(digitIndex));
            compare.setStats(initialCheerfulness: digStat);
        }
        else if (stat.StartsWith("Fatigue"))
        {
            actual.setStats(initialFatigue: MainCharacter.GetStats().Fatigue);
            int digStat = int.Parse(rule.Substring(digitIndex));
            compare.setStats(initialFatigue: digStat);
        }
        else if (stat.StartsWith("Money"))
        {
            actual.setStats(initialMoney: MainCharacter.GetStats().Money);
            decimal digStat = decimal.Parse(rule.Substring(digitIndex));
            compare.setStats(initialMoney: digStat);
        }
        else
        {
            Console.WriteLine("WRONG STAT");
            return false;
        }

        if (rule[signIndex].Equals('>'))
        {
            return !Stats.less(actual, compare);

        }
        else
        {
            return Stats.less(actual, compare);
        }

    }

    public void DisplayDict(Dictionary<string, string> dict)
    {
        foreach (var el in dict)
        {
            Console.WriteLine($"{el.Key} {el.Value}");
        }
    }
    public void DrawPlayMenu()
    {
        Console.WriteLine("Игровое Меню:");
        MainCharacter.DisplayInfo();
        Console.WriteLine("==================================");
        foreach (var record in RawConfig)
        {
            Console.WriteLine(record.Value.title);
        }
        Console.WriteLine("8.Сохранить игру");
        Console.WriteLine("9.Выход");

        Console.WriteLine("==================================");

        Console.WriteLine(">");

    }

    public void limitStats()
    {
        var updateStats = MainCharacter.GetStats();
        if (updateStats.Mana > defaultConfig.MAX_MANA)
        {
            updateStats.Mana = defaultConfig.MAX_MANA;
        }
        else if (updateStats.Mana < defaultConfig.MIN_MANA)
        {
            updateStats.Mana = defaultConfig.MIN_MANA;
        }
        if (updateStats.Fatigue > defaultConfig.MAX_FATIGUE)
        {
            updateStats.Mana = defaultConfig.MAX_MANA;
        }
        else if (updateStats.Fatigue < defaultConfig.MIN_FATIGUE)
        {
            updateStats.Fatigue = defaultConfig.MIN_FATIGUE;
        }
        if (updateStats.Cheerfulness > defaultConfig.MAX_CHEER)
        {
            updateStats.Cheerfulness = updateStats.Cheerfulness;
        }
        else if (updateStats.Cheerfulness < defaultConfig.MIN_CHEER)
        {
            updateStats.Cheerfulness = defaultConfig.MIN_CHEER;
        }
        if (updateStats.Health > defaultConfig.MAX_HEALTH)
        {
            updateStats.Health = defaultConfig.MAX_HEALTH;
        }
        else if (updateStats.Health < defaultConfig.MIN_HEALTH)
        {
            updateStats.Health = defaultConfig.MIN_HEALTH;
        }

        MainCharacter.setStats(updateStats);
    }
    public void addStats(string actionName)
    {
        var MainStats = RawConfig[actionName].main.Stats;

        if (RawConfig[actionName].main.Cond == null)
        {
            MainCharacter.ChangeStats(MainStats);
        }
        else
        {
            foreach (var c in RawConfig[actionName].main.Cond)
            {
                if (!checkStat(c.Value, c.Key))
                {
                    Console.WriteLine(RawConfig[actionName].main.text);
                    return;
                }
            }
            MainCharacter.DisplayInfo();
            MainCharacter.ChangeStats(MainStats);

            Console.WriteLine("Stats changed");
            MainCharacter.DisplayInfo();
        }

        if (RawConfig[actionName].Extra != null)
        {
            foreach (var record in RawConfig[actionName].Extra)
            {
                var isMatching = true;
                foreach (var stat in record.Cond)
                {
                    if (!checkStat(stat.Value, stat.Key))
                    {
                        isMatching = false;
                    }
                }
                if (isMatching)
                {
                    MainCharacter.ChangeStats(record.Stats);
                    Console.WriteLine("Stats changed");
                    MainCharacter.DisplayInfo();
                }
                else
                {
                    Console.WriteLine(record.text);
                }

            }
        }
        limitStats();

    }
    public void SaveGame()
    {
        var SaveStats = MainCharacter.GetStats();
        string newSave = JsonSerializer.Serialize(SaveStats);

        Console.WriteLine("Введите имя файла");
        Console.WriteLine(">");
        string newFile = Console.ReadLine();
        File.WriteAllText(newFile, newSave);

        Console.WriteLine("Файл успешно сохранен!");
    }
    public void Play()
    {
        PChoice choice = PChoice.Wait;

        while (choice != PChoice.Finish)
        {
            DrawPlayMenu();
            MainCharacter.DisplayInfo();
            choice = (PChoice)(int.Parse(Console.ReadLine()));
            //try execpt
            switch (choice)
            {
                case PChoice.Work:
                    addStats("work");
                    break;
                case PChoice.Bar:
                    addStats("bar");
                    break;
                case PChoice.Nature:
                    addStats("nature");
                    break;
                case PChoice.SoapWine:
                    addStats("soapWine");
                    break;
                case PChoice.MarginalDrink:
                    addStats("marginalDrink");
                    break;
                case PChoice.Metro:
                    addStats("metro");
                    break;
                case PChoice.Sleep:
                    addStats("sleep");
                    break;
                case PChoice.SaveGame:
                    SaveGame();
                    break;

            }
            if (MainCharacter.GetStats().Health == defaultConfig.MIN_HEALTH)
            {
                Console.WriteLine("Валера покинул этот мир не по своей воле(");
                return;
            }
            else if (MainCharacter.GetStats().Cheerfulness == defaultConfig.MIN_CHEER)
            {
                Console.WriteLine("Валера устал и решил, что с него достаточно(");
                return;
            }


        }

    }

    private void LoadGame()
    {
        Console.WriteLine("Введите имя файла:");
        var saveGame = Console.ReadLine();
        string json = File.ReadAllText(saveGame);
        Stats newStats = JsonSerializer.Deserialize<Stats>(json)!;
        MainCharacter.setStats(newStats);
    }

    void NewGame()
    {
        MainCharacter.setStats(defaultConfig.defaultStats);
    }
    public void MainMenu()
    {
        string jsonString = File.ReadAllText("conf.json");
        RawConfig = JsonSerializer.Deserialize<Dictionary<string, ActionConfig>>(jsonString)!;
        int choice = 10;
        while (choice != 3)
        {
            DrawMainMenu();
            choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    NewGame();
                    Play();
                    break;
                case 2:
                    LoadGame();
                    Play();
                    break;
                case 3:
                    return;
            }
        }

    }
}

public class Stats
{


    public int Health { get; set; }
    public int Mana { get; set; }
    public int Cheerfulness { get; set; }
    public int Fatigue { get; set; }
    public decimal Money { get; set; }
    public Stats(int initialHealth = 0, int initialMana = 0, int initialCheerfulness = 0, int initialFatigue = 0, decimal initialMoney = 0.0m)
    {
        Health = initialHealth;
        Mana = initialMana;
        Cheerfulness = initialCheerfulness;
        Fatigue = initialFatigue;
        Money = initialMoney;
    }

    public Stats()
    {
        Health = 0;
        Mana = 0;
        Cheerfulness = 0;
        Fatigue = 0;
        Money = 0;
    }

    public static Stats operator +(Stats a, Stats b)
    {
        var newStats = new Stats(a.Health + b.Health, a.Mana + b.Mana, a.Cheerfulness + b.Cheerfulness, a.Fatigue + b.Fatigue, a.Money + b.Money);
        return newStats;
    }

    public void setStats(int initialHealth = 0, int initialMana = 0, int initialCheerfulness = 0, int initialFatigue = 0, decimal initialMoney = 0.0m)
    {
        Health = initialHealth;
        Mana = initialMana;
        Cheerfulness = initialCheerfulness;
        Fatigue = initialFatigue;
        Money = initialMoney;
    }
    public void setStats(Stats stats)
    {
        Health = stats.Health;
        Mana = stats.Mana;
        Cheerfulness = stats.Cheerfulness;
        Fatigue = stats.Fatigue;
        Money = stats.Money;
    }


    public void displayNotZero()
    {
        if (Health != 0)
        {
            Console.WriteLine($"Health: {Health}");
        }
        if (Mana != 0)
        {
            Console.WriteLine($"Mana: {Mana}");
        }
        if (Cheerfulness != 0)
        {
            Console.WriteLine($"Cherfulness: {Cheerfulness}");
        }
        if (Fatigue != 0)
        {
            Console.WriteLine($"Fatihue: {Fatigue}");
        }
        if (Money != 0)
        {
            Console.WriteLine($"Money: {Money}");
        }
    }
    public static bool less(Stats a, Stats b)
    {
        if (a.Health > b.Health)
        {
            return false;
        }
        if (a.Mana > b.Mana)
        {
            return false;
        }
        if (a.Cheerfulness > b.Cheerfulness)
        {
            return false;
        }
        if (a.Fatigue > b.Fatigue)
        {
            return false;
        }
        if (a.Money > b.Money)
        {
            return false;
        }
        Console.WriteLine("return true");
        return true;
    }
}

public class Valera
{

    private Stats MainStats;

    public Stats GetStats() { return MainStats; }
    public Valera(int initialHealth, int initialMana, int initialCheerfulness, int initialFatigue, decimal initialMoney)
    {
        MainStats = new Stats(initialHealth, initialMana, initialCheerfulness, initialFatigue, initialMoney);


    }
    public Valera()
    {
        MainStats = new Stats();
    }

    public void setStats(Stats newStats)
    {
        MainStats = newStats;
    }
    public void DisplayInfo()
    {
        Console.WriteLine($"Здоровье: {MainStats.Health}");
        Console.WriteLine($"Мана (Алкоголь в крови): {MainStats.Mana}");
        Console.WriteLine($"Жизнерадостность: {MainStats.Cheerfulness}");
        Console.WriteLine($"Усталость: {MainStats.Fatigue}");
        Console.WriteLine($"Деньги: {MainStats.Money:C}");
    }

    public void ChangeStats(Stats change)
    {
        MainStats = MainStats + change;
    }




}