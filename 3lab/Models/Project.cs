﻿using System.ComponentModel;

namespace lab3.Models
{
    public class Project
    {
        public int Id { get; set; }

        [DisplayName("Название проекта")]
        public string Name { get; set; }

        [DisplayName("Название компании заказчика")]
        public string CustomCompanyName { get; set; }

        [DisplayName("Название компании исполнителя")]
        public string ExecutorCompanyName { get; set; }

        [DisplayName("Директор")]
        public int DirectorId { get; set; }

        [DisplayName("Работники")]
        public List<int> WorkersId { get; set; } = new List<int>();

        [DisplayName("Приоритет")]
        public int Priority { get; set; }

        [DisplayName("Дата начала проекта")]
        public DateTime StartTime { get; set; } = DateTime.Now;

        [DisplayName("Дата окончания проекта")]
        public DateTime EndTime { get; set; }

    }
}
