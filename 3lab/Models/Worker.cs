﻿using System.ComponentModel;

namespace lab3.Models
{
    public class Worker
    {
        [DisplayName("ID")]
        public int Id { get; set; }
        
        [DisplayName("Имя")]
        public string Name { get; set; }
        
        [DisplayName("Фамилия")]
        public string Surname { get; set; }
        
        [DisplayName("Отчество")]
        public string Patronymic { get; set; }
        
        [DisplayName("Почта")]
        public string Email { get; set; }

        
    }
}
