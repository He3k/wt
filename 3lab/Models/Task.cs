﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace lab3.Models
{
    public class Task
    {
        public int Id { get; set; }

        [DisplayName("Название задачи")]
        public string? Name { get; set; }

        [DisplayName("Название проекта")]
        public int ProjectId { get; set; }

        [DisplayName("Автор задачи")]
        public int AuthorId { get; set; }

        [DisplayName("Исполнитель")]
        public int WorkerId { get; set; }

        [Display(Name = "Статус")]
        public TaskStatus Status { get; set; }

        [DisplayName("Комментарий")]
        public string? Comment { get; set; }

        [DisplayName("Приоритет")]
        public int Priority {  get; set; }

    }
    public enum TaskStatus
    {
        [Display(Name = "Сделать")] ToDo,
        [Display(Name = "В процессе")] InProgress,
        [Display(Name = "Сделано")] Done
    }
}
