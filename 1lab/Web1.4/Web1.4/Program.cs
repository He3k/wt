﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;

internal class Program
{
    static void Main()
    {
        string csvFilePath = "../../../nauka.csv";
        ScienceReport report = new ScienceReport();
        using (TextFieldParser parser = new TextFieldParser(csvFilePath))
        {
            // Set the delimiter for the CSV file
            parser.Delimiters = new string[] { "," };
            string[] fields = parser.ReadFields();

            // Read the fields from the CSV file
            while (!parser.EndOfData)
            {
                // Read a row from the CSV file
                fields = parser.ReadFields();

                // Process the fields as needed
                Record rec = new Record();
                rec.Year = int.Parse(fields[0]);
                rec.Count = int.Parse(fields[1]);
                report.Add(rec);
            }
        }
        Record recrd = report.GetMaxCount();
        Console.WriteLine("Max: " + recrd.Year + " " + recrd.Count);
        recrd = report.GetMinCount();
        Console.WriteLine("Min: " + recrd.Year + " " + recrd.Count);

        Console.WriteLine("Average: " + report.GetAverageCount());
        Console.WriteLine("Dispersion: " + report.GetDispersion());
        Console.WriteLine("Press Enter...");
        Console.Read();
    }

    class Record
    {
        public Record()
        {
            Year = 0;
            Count = 0;
        }
        public int Year;
        public int Count;

    }
    class ScienceReport
    {
        public void Add(Record record)
        {
            records.Add(record);
        }
        public ScienceReport()
        {
            records = new List<Record>();
        }
        private List<Record> records;
        public Record GetMaxCount()
        {
            if (records == null)
            {
                return new Record();
            }
            Record max = records[0];
            foreach (Record record in records)
            {
                if (record.Count > max.Count)
                {
                    max = record;
                }
            }
            return max;
        }
        public Record GetMinCount()
        {
            if (records == null)
            {
                return new Record();
            }
            Record min = records[0];
            foreach (Record record in records)
            {
                if (record.Count < min.Count)
                {
                    min = record;
                }
            }
            return min;
        }

        public double GetAverageCount()
        {
            if (records == null)
            {
                return -1;
            }
            float sum = 0;
            foreach (Record record in records)
            {
                sum += record.Count;
            }
            return sum / records.Count;
        }
        public double GetDispersion()
        {
            double average = GetAverageCount();
            double dispersion = 0;
            foreach (Record record in records)
            {
                dispersion += Math.Pow(record.Count - average, 2);
            }
            dispersion = Math.Sqrt(dispersion / records.Count);

            return dispersion;
        }
    }

}


