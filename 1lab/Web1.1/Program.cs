﻿using System;

public class Program
{
    public static void Main()
    {
        double degree, out_degree;
        char letter, letter_result;

        Console.WriteLine("Enter value");
        degree = Single.Parse(Console.ReadLine());
        Console.WriteLine("Enter name input(C,K,F)");
        letter = Convert.ToChar(Console.ReadLine());
        Console.WriteLine("Enter name output(C,K,F)");
        letter_result = Convert.ToChar(Console.ReadLine());


        if (letter == 'f' | letter == 'F')
        {
            switch (letter_result)
            {
                case 'f':
                case 'F':
                    out_degree = degree;
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'c':
                case 'C':
                    out_degree = ((degree - 32) / 1.8);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'k':
                case 'K':
                    out_degree = ((degree + 459.67) / 1.8);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
            }
        }
        if (letter == 'c' | letter == 'C')
        {
            switch (letter_result)
            {
                case 'f':
                case 'F':
                    out_degree = ((degree * 1.8) + 32);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'c':
                case 'C':
                    out_degree = degree;
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'k':
                case 'K':
                    out_degree = (degree + 273.15);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
            }
        }
        if (letter == 'k' | letter == 'K')
        {
            switch (letter_result)
            {
                case 'f':
                case 'F':
                    out_degree = ((degree * 1.8) - 459.67);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'c':
                case 'C':
                    out_degree = (degree - 273.15);
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
                case 'k':
                case 'K':
                    out_degree = degree;
                    Console.WriteLine($"{out_degree} {letter_result}");
                    break;
            }
        }


    }
}
