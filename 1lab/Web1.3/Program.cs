﻿using System;

class Program
{
    static void Main()
    {
        Console.WriteLine("Please, enter count months:");
        int months = Convert.ToInt32(Console.ReadLine());

        int prevMonthCount = 0;
        int currentMonthCount = 1;
        int nextMonthCount;

        for (int i = 2; i <= months; i++)
        {
            nextMonthCount = prevMonthCount + currentMonthCount;
            prevMonthCount = currentMonthCount;
            currentMonthCount = nextMonthCount;
        }

        Console.WriteLine("Count par rabbitov after months" + months + ": " + currentMonthCount);
    }
}
