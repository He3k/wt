﻿using System;

public class Program
{
    public static void Main()
    {
        // Объявление переменной для хранения слова
        string text;

        // Приглашение пользователя ввести слово
        Console.WriteLine("Enter word");
        text = Console.ReadLine();

        // Проверка, является ли введенное слово палиндромом
        if (IsPalindrome(text))
        {
            Console.WriteLine("Word palindrom");
        }
        else
        {
            Console.WriteLine("Word no palindrom");
        }
    }

    // Функция для проверки, является ли слово палиндромом
    private static bool IsPalindrome(string word)
    {
        int left = 0;
        int right = word.Length - 1;

        // Продолжаем сравнивать символы с обоих концов
        while (left < right)
        {
            // Если символы не совпадают, это не палиндром
            if (word[left] != word[right])
            {
                return false;
            }

            // Переходим к следующей паре символов
            left++;
            right--;
        }

        // Если цикл завершен, то слово является палиндромом
        return true;
    }
}